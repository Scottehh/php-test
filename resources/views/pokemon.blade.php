<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Pokedex</title>
    <base href="{{URL::to('/')}}" <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }

        .navbar-brand {
            font-weight: 600;
        }
    </style>
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">UKFast Pokedex (Scott Evans)</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
            </div>
            <!--/.navbar-collapse -->
        </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <img src="{{$pokemon['img'][0]}}" />
            <img src="{{$pokemon['img'][1]}}" />
            <h1><small>#{{$pokemon['id']}}</small> {{ $pokemon['name'] }}</h1>
            <p>{{ $pokemon['species'] }}</p>
        </div>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-12">
                <h2>Information</h2>
                <table class="table table-striped table-bordered" style="width:100%">
                    <tbody>
                        <tr>
                            <th style='width: 100px;'>Height</th>
                            <td>{{ $pokemon['height'] }}</td>
                        </tr>
                        <tr>
                            <th>Weight</th>
                            <td>{{ $pokemon['weight'] }}</td>
                        </tr>
                        <tr>
                            <th>Stats</th>
                            <td>
                                @foreach($pokemon['stats'] as $s)
                                    (<strong>{{$s->base_stat}}</strong>) {{strtoupper($s->stat->name)}}<br />
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Types</th>
                            <td>
                                @foreach($pokemon['types'] as $s)
                                    {{strtoupper($s->type->name)}}<br />
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Abilities</th>
                            <td>
                                @foreach($pokemon['abilities'] as $a)
                                    Slot #{{$a->slot}}: {{ucfirst($a->ability->name)}}<br />
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Moves</th>
                            <td>
                                @foreach($pokemon['moves'] as $m)
                                    {{ucfirst($m->move->name)}},
                                @endforeach
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
        <hr>
    </div>
    <!-- /container -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')
    </script>

    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>