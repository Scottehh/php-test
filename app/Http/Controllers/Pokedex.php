<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Kevinrob\GuzzleCache\CacheMiddleware;

use Illuminate\Support\Facades\Cache;
use Kevinrob\GuzzleCache\Strategy\PrivateCacheStrategy;
use Kevinrob\GuzzleCache\Storage\LaravelCacheStorage;

use Illuminate\Http\Request;

class Pokedex extends Controller
{
    protected $client;

    public function __construct(){
        $stack = HandlerStack::create();
        $stack->push(
            new CacheMiddleware(
              new PrivateCacheStrategy(
                new LaravelCacheStorage(
                  Cache::store('file')
                )
              )
            ),
            'cache'
          );
        $this->client = new Client(['handler' => $stack, "base_uri" => "http://pokeapi.co/api/v2/"]);

        // $cachePlugin = new CachePlugin(array(
        //     'storage' => new DefaultCacheStorage(
        //         new DoctrineCacheAdapter(
        //             new FilesystemCache('/var/www/UKFast/storage/app/cache')
        //         )
        //     )
        // ));
        // $this->client = new \GuzzleHttp\Client(["base_uri" => "http://pokeapi.co/api/v2/"]);
        // $this->client->addSubscriber($cachePlugin);
    }
    public function index() {
        $response = $this->callAPI('pokemon', 1000);
        return view("home", ["pokemon" => $response->results]);

        // return view("home");
    }

    public function pokemon($name){ 
        $response = $this->callAPI('pokemon/'.$name, false);
        $species = $this->callAPI($response->species->url, false);

        // dd($species);

        $pokemon = array(
            "id" => $response->id,
            "name" => ucfirst($response->name),
            "weight" => $response->weight,
            "height" => $response->height,
            "img" => [$response->sprites->front_default, $response->sprites->back_default],
            "species" => $species->genera[2]->genus,
            "stats" => $response->stats,
            "types" => $response->types,
            "abilities" => $response->abilities,
            "moves" => $response->moves
        );
        return view("pokemon", ["pokemon" => $pokemon]);
    }
 
    private function callAPI($endpoint, $limit = 20){
        if($limit !== false && $limit > 0){
            $endpoint .= "?limit={$limit}";
        }

        $res = $this->client->request('GET', $endpoint);
        if($res->getStatusCode() == 200){
            // Request Sucessful
            $response = $res->getBody();
            return json_decode($response);
        }else{
            dump("HTTP Request Failure");
            dd($res);
        }

    }
}
